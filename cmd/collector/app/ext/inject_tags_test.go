package ext

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestParseTags(t *testing.T) {
	assert.Equal(t, parseTags(""), map[string]string{})
	assert.Equal(t, parseTags("aaaaa"), map[string]string{})
	assert.Equal(t, parseTags("kkk=vvv"), map[string]string{"kkk": "vvv"})
	assert.Equal(t, parseTags("kkk=vvv,"), map[string]string{"kkk": "vvv"})
	assert.Equal(t, parseTags("kkk= vvv,"), map[string]string{"kkk": "vvv"})
	assert.Equal(t, parseTags("kkk=vvv,aaa"), map[string]string{"kkk": "vvv"})
	assert.Equal(t, parseTags("kkk=vvv,kkk=bbb"), map[string]string{"kkk": "bbb"})
	assert.Equal(t, parseTags("kkk=vvv,aaa=bbb"), map[string]string{"kkk": "vvv", "aaa": "bbb"})
	assert.Equal(t, parseTags("kkk=vvv, aaa=bbb"), map[string]string{"kkk": "vvv", "aaa": "bbb"})
}
