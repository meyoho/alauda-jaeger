package ext

import (
	"github.com/jaegertracing/jaeger/model"
	"os"
	"strings"
)

const (
	tagsEnv = "JAEGER_TAGS"
)

var (
	toAddTags []model.KeyValue
)

func InjectTags(spans []*model.Span) {
	for _, span := range spans {
		appendTags(span)
	}
}

func appendTags(span *model.Span) {
	for _, kv := range toAddTags {
		span.Tags = append(span.Tags, kv)
	}
}

func trimStr(s string) string {
	return strings.Trim(s, " ")
}

func parseTags(data string) (result map[string]string) {
	result = map[string]string{}
	for _, kvStr := range strings.Split(data, ",") {
		kvStr = trimStr(kvStr)
		kv := strings.Split(kvStr, "=")
		if len(kv) != 2 {
			continue
		}
		result[trimStr(kv[0])] = trimStr(kv[1])
	}
	return
}

func init() {
	kvs := parseTags(os.Getenv(tagsEnv))
	for k, v := range kvs {
		toAddTags = append(toAddTags, model.KeyValue{Key: k, VType: model.ValueType_STRING, VStr: v})
	}
}
