// Copyright (c) 2017 Uber Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// site-prefix.js must be the first import of the main webpack entrypoint
// becaue it configures the webpack publicPath.
/* eslint-disable import/first */
import './site-prefix';

import React from 'react';
import ReactDOM from 'react-dom';
import { document } from 'global';

import JaegerUIApp from './components/App';
import { context as trackingContext } from './utils/tracking';
import { LocaleProvider } from 'antd';
import zhCN from 'antd/lib/locale-provider/zh_CN';
import enUS from 'antd/lib/locale-provider/en_US';

import zhData from 'react-intl/locale-data/zh';
import enData from 'react-intl/locale-data/en';
import zhMessages from './locales/zh.json';
import enMessages from './locales/en.json';
import { addLocaleData, IntlProvider } from 'react-intl';

addLocaleData([...zhData, ...enData]);

// these need to go after the App import
/* eslint-disable import/first */
import 'u-basscss/css/flexbox.css';
import 'u-basscss/css/layout.css';
import 'u-basscss/css/margin.css';
import 'u-basscss/css/padding.css';
import 'u-basscss/css/position.css';
import 'u-basscss/css/typography.css';
import store from 'store';

window.parent.postMessage('jaegerInitialized', '*');
let lang = 'zh';
let rendered = false;
window.addEventListener('message', (e)=>{
  if(e.data && e.data.source === 'diablo'){
    const services = e.data.services;
    const selectedService = e.data.selectedService;
    const namespace = e.data.namespace;
    localStorage.setItem('services', JSON.stringify(services));
    localStorage.setItem('condition', JSON.stringify({
      service: selectedService ? `${selectedService}.${namespace}` : '',
      lookback: e.data.lookback || '',
    }));
    if(selectedService){
      store.set('lastSearch', { service: `${selectedService}.${namespace}`});
    }
    if(e.data.namespace) {
      localStorage.setItem('alauda_service_namespace', namespace);
    }
    lang = e.data.lang;
    if(lang === 'zh'){
      require('moment/locale/zh-cn');
    }
    render(lang);
  }
})

function render(lang){
  if(rendered){
    window.location.reload();
  }
  const UI_ROOT_ID = 'jaeger-ui-root';
  const messages = lang === 'en'?enMessages:zhMessages;
  const antdLocale = lang === 'en'?enUS:zhCN;
  if (trackingContext) {
    trackingContext.context(() => {
      ReactDOM.render(
        <LocaleProvider locale={antdLocale}>
          <IntlProvider locale={lang==='zh'?'zh-CN':'en'} messages={messages}>
            <JaegerUIApp />
          </IntlProvider>
        </LocaleProvider>, 
        document.getElementById(UI_ROOT_ID)
      );
    });
  } else {
    ReactDOM.render(
      <LocaleProvider locale={antdLocale}>
        <IntlProvider locale={lang==='zh'?'zh-CN':'en'} messages={messages}>
          <JaegerUIApp />
        </IntlProvider>
      </LocaleProvider>, 
      document.getElementById(UI_ROOT_ID)
    );
  }
  rendered = true;
}


