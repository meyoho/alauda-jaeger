import { defineMessages } from 'react-intl';

export const translateKeys = {
  searchForm: defineMessages({
    service: {id: 'search.fields.service'},
    operation: {id: 'search.fields.operation'},
    tags: {id: 'search.fields.tags'},
    lookback: {id: 'search.fields.lookback'},
    starttime: {id: 'search.fields.start_time'},
    endtime: {id: 'search.fields.end_time'},
    minduration: {id: 'search.fields.min_duration'},
    maxduration: {id: 'search.fields.max_duration'},
    limitresults: {id: 'search.fields.limit_results'},
    selectService: {id: 'placeholder.select_service'},
    selectOperation: {id: 'placeholder.select_operation'},
    buttonSearch: {id: 'button.search'},
    buttonReset: {id: 'button.reset'},
    custom: {id: 'lookback.custom'},
    '10minutes': {id: 'lookback.10_minutes'},
    '30minutes': {id: 'lookback.30_minutes'},
    '1hour': {id: 'lookback.1_hour'},
    '2hours': {id: 'lookback.2_hours'},
    '3hours': {id: 'lookback.3_hours'},
    '6hours': {id: 'lookback.6_hours'},
    '12hours': {id: 'lookback.12_hours'},
    '24hours': {id: 'lookback.24_hours'},
    '1day': {id: 'lookback.1_day'},
    '3days': {id: 'lookback.3_days'},
    '7days': {id: 'lookback.7_days'},
    '30days': {id: 'lookback.30_days'},
    conditions: {id: 'search.conditions'},
  }),
  searchResult: defineMessages({
    empty: {id: 'search.empty'}
  }),
  searchWarn: defineMessages({
    warn: {id: 'search.warn'}
  }),
  tracePageHeader: defineMessages({
    back: {id: 'back'}
  })
}