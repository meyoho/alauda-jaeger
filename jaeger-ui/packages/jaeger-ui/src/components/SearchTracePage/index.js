// Copyright (c) 2017 Uber Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import React, { Component } from 'react';
import { Col, Row } from 'antd';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import store from 'store';

import * as jaegerApiActions from '../../actions/jaeger-api';
import { actions as traceDiffActions } from '../TraceDiff/duck';
import SearchForm from './SearchForm';
import SearchResults, { sortFormSelector } from './SearchResults';
import ErrorMessage from '../common/ErrorMessage';
import LoadingIndicator from '../common/LoadingIndicator';
import { fetchedState } from '../../constants';
import { sortTraces } from '../../model/search';
import getLastXformCacher from '../../utils/get-last-xform-cacher';
import prefixUrl from '../../utils/prefix-url';
import { translateKeys } from '../../translate';
import { FormattedMessage } from 'react-intl';
import './index.css';

const messages = translateKeys.searchWarn;

// export for tests
export class SearchTracePageImpl extends Component {
  componentDidMount() {
    const {
      diffCohort,
      fetchMultipleTraces,
      searchTraces,
      urlQueryParams,
      fetchServices,
      fetchServiceOperations,
    } = this.props;
    if (urlQueryParams.service || urlQueryParams.traceID) {
      searchTraces(urlQueryParams);
    }
    const needForDiffs = diffCohort.filter(ft => ft.state == null).map(ft => ft.id);
    if (needForDiffs.length) {
      fetchMultipleTraces(needForDiffs);
    }
    fetchServices();
    const { service } = store.get('lastSearch') || {};
    if (service && service !== '') {
      const namespace = localStorage.getItem('alauda_service_namespace');
      let serviceName = service;
      if(!service.includes(namespace)){
        serviceName = `${service}.${namespace}`
      }
      fetchServiceOperations(serviceName);
    }
  }

  goToTrace = traceID => {
    this.props.history.push(prefixUrl(`/trace/${traceID}`));
  };

  submitFn = (event) => {
    this.submitting = event;
  }

  render() {
    const {
      cohortAddTrace,
      cohortRemoveTrace,
      diffCohort,
      errors,
      isHomepage,
      loadingServices,
      loadingTraces,
      maxTraceDuration,
      services,
      traceResults,
    } = this.props;
    const showErrors = errors && !loadingTraces;
    return (
        <Row className="main">
          <Col span={24} className="search-form">
          {!loadingServices && services && (
            <SearchForm submitState={this.submitFn} services={services} />
          )}
          {(loadingServices || !services) && (
            <div className="loading">
              <LoadingIndicator />
            </div>
          )}
          </Col>
          <Col span={24} className="search-result">
            {this.submitting &&(
              <div className="js-test-error-message warn">
                  <FormattedMessage {...messages.warn}/>
              </div>
            )}
            {showErrors && (
              <div className="js-test-error-message">
                <h2>There was an error querying for traces:</h2>
                {errors.map(err => <ErrorMessage key={err.message} error={err} />)}
              </div>
            )}
            {!showErrors && (
              <SearchResults
                cohortAddTrace={cohortAddTrace}
                goToTrace={this.goToTrace}
                loading={loadingTraces}
                maxTraceDuration={maxTraceDuration}
                cohortRemoveTrace={cohortRemoveTrace}
                diffCohort={diffCohort}
                skipMessage={isHomepage}
                traces={traceResults}
              />
            )}
          </Col>
        </Row>
    );
  }
}

SearchTracePageImpl.propTypes = {
  isHomepage: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  traceResults: PropTypes.array,
  diffCohort: PropTypes.array,
  cohortAddTrace: PropTypes.func,
  cohortRemoveTrace: PropTypes.func,
  maxTraceDuration: PropTypes.number,
  loadingServices: PropTypes.bool,
  loadingTraces: PropTypes.bool,
  urlQueryParams: PropTypes.shape({
    service: PropTypes.string,
    limit: PropTypes.string,
  }),
  services: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      operations: PropTypes.arrayOf(PropTypes.string),
    })
  ),
  searchTraces: PropTypes.func,
  history: PropTypes.shape({
    push: PropTypes.func,
  }),
  fetchMultipleTraces: PropTypes.func,
  fetchServiceOperations: PropTypes.func,
  fetchServices: PropTypes.func,
  errors: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.string,
    })
  ),
};

const stateTraceXformer = getLastXformCacher(stateTrace => {
  const { traces: traceMap, search } = stateTrace;
  const { results, state, error: traceError } = search;
  const loadingTraces = state === fetchedState.LOADING;
  const traces = results.map(id => traceMap[id].data);
  const maxDuration = Math.max.apply(null, traces.map(tr => tr.duration));
  return { traces, maxDuration, traceError, loadingTraces };
});

const stateTraceDiffXformer = getLastXformCacher((stateTrace, stateTraceDiff) => {
  const { traces } = stateTrace;
  const { cohort } = stateTraceDiff;
  return cohort.map(id => traces[id] || { id });
});

const sortedTracesXformer = getLastXformCacher((traces, sortBy) => {
  const traceResults = traces.slice();
  sortTraces(traceResults, sortBy);
  return traceResults;
});

const stateServicesXformer = getLastXformCacher(stateServices => {
  const {
    loading: loadingServices,
    services: serviceList,
    operationsForService: opsBySvc,
    error: serviceError,
  } = stateServices;
  const services =
    serviceList &&
    serviceList.map(name => ({
      name,
      operations: opsBySvc[name] || [],
    }));
  return { loadingServices, services, serviceError };
});

// export to test
export function mapStateToProps(state) {
  const query = queryString.parse(state.router.location.search);
  const isHomepage = !Object.keys(query).length;
  const { traces, maxDuration, traceError, loadingTraces } = stateTraceXformer(state.trace);
  const diffCohort = stateTraceDiffXformer(state.trace, state.traceDiff);
  const { loadingServices, services, serviceError } = stateServicesXformer(state.services);
  const errors = [];
  if (traceError) {
    errors.push(traceError);
  }
  if (serviceError) {
    errors.push(serviceError);
  }
  const sortBy = sortFormSelector(state, 'sortBy');
  const traceResults = sortedTracesXformer(traces, sortBy);
  return {
    diffCohort,
    isHomepage,
    loadingServices,
    loadingTraces,
    services,
    traceResults,
    errors: errors.length ? errors : null,
    maxTraceDuration: maxDuration,
    sortTracesBy: sortBy,
    urlQueryParams: query,
  };
}

function mapDispatchToProps(dispatch) {
  const { fetchMultipleTraces, fetchServiceOperations, fetchServices, searchTraces } = bindActionCreators(
    jaegerApiActions,
    dispatch
  );
  const { cohortAddTrace, cohortRemoveTrace } = bindActionCreators(traceDiffActions, dispatch);
  return {
    cohortAddTrace,
    cohortRemoveTrace,
    fetchMultipleTraces,
    fetchServiceOperations,
    fetchServices,
    searchTraces,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchTracePageImpl);
