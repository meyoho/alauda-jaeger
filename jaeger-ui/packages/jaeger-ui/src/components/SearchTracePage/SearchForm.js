// Copyright (c) 2017 Uber Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import * as React from 'react';
import { Form, Input, Button, Popover, Select, Row, Col, DatePicker } from 'antd';
import logfmtParser from 'logfmt/lib/logfmt_parser';
import { stringify as logfmtStringify } from 'logfmt/lib/stringify';
import moment from 'moment';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import IoHelp from 'react-icons/lib/io/help';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import store from 'store';
import { FormattedMessage, injectIntl, intlShape } from 'react-intl';

import * as markers from './SearchForm.markers';
import { trackFormInput } from './SearchForm.track';
import * as jaegerApiActions from '../../actions/jaeger-api';
import reduxFormFieldAdapter from '../../utils/redux-form-field-adapter';
import { DEFAULT_OPERATION, DEFAULT_LIMIT, DEFAULT_LOOKBACK } from '../../constants/search-form';

import './SearchForm.css';
import { translateKeys } from '../../translate';


const messages = translateKeys.searchForm;

const FormItem = Form.Item;
const Option = Select.Option;

const AdaptedInput = reduxFormFieldAdapter(Input);
const AdaptedSelect = reduxFormFieldAdapter(Select);
// const AdaptedVirtualSelect = reduxFormFieldAdapter(VirtSelect, option => (option ? option.value : null));
const AdaptedDatePikcer = reduxFormFieldAdapter(DatePicker);

export function getUnixTimeStampInMSFromForm({ startDate, endDate }) {
  return {
    start: `${startDate.valueOf()}000`,
    end: `${endDate.valueOf()}000`,
  };
}

export function convTagsLogfmt(tags) {
  if (!tags) {
    return null;
  }
  const data = logfmtParser.parse(tags);
  Object.keys(data).forEach(key => {
    const value = data[key];
    // make sure all values are strings
    // https://github.com/jaegertracing/jaeger/issues/550#issuecomment-352850811
    if (typeof value !== 'string') {
      data[key] = String(value);
    }
  });
  return JSON.stringify(data);
}

export function traceIDsToQuery(traceIDs) {
  if (!traceIDs) {
    return null;
  }
  return traceIDs.split(',');
}

export function convertQueryParamsToFormDates({ start, end }) {
  let queryStartDate;
  let queryEndDate;
  if (start) {
    const startUnixNs = parseInt(start, 10);
    queryStartDate = moment(startUnixNs/1000);
  }
  if (end) {
    const endUnixNs = parseInt(end, 10);
    queryEndDate = moment(endUnixNs/1000);
  }
  return {
    queryStartDate,
    queryEndDate,
  };
}
const dateForm = {
  endValue: null,
  startValue: null,
}

export function submitForm(fields, searchTraces) {
  const {
    resultsLimit,
    service,
    startDate,
    endDate,
    operation,
    tags,
    minDuration,
    maxDuration,
    lookback,
  } = fields;
  // Note: traceID is ignored when the form is submitted
  store.set('lastSearch', { service, operation });

  let start;
  let end;
  if (lookback !== 'custom') {
    const unit = lookback.substr(-1);
    const now = new Date();
    start =
      moment(now)
        .subtract(parseInt(lookback, 10), unit)
        .valueOf() * 1000;
    end = moment(now).valueOf() * 1000
  } else {
    const times = getUnixTimeStampInMSFromForm({
      startDate: dateForm.startValue || startDate,
      endDate: dateForm.endValue || endDate,
    }); 

    start = times.start;
    end = times.end;
  }

  trackFormInput(resultsLimit, operation, tags, minDuration, maxDuration, lookback);

  searchTraces({
    service,
    operation: operation !== DEFAULT_OPERATION ? operation : undefined,
    limit: resultsLimit,
    lookback,
    start,
    end,
    tags: convTagsLogfmt(tags) || undefined,
    minDuration: minDuration || null,
    maxDuration: maxDuration || null,
  });
}

export class SearchFormImpl extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = {
      scrollTop: 0,
      startValue: null,
      endValue: null,
      endOpen: false,
    }
  }

  componentDidMount(){
    this.setInitDate()
    window.addEventListener('scroll', this.handleScroll.bind(this), false)
  }

  componentWillUnmount(){
    window.removeEventListener('scroll', this.handleScroll, false);
    this.setState = ()=>undefined;
  }

  setInitDate() {
    const now = new Date();
    const start =
      moment(now)
        .subtract(parseInt('1h', 10), 'h')
        .valueOf() * 1000;
    const end = moment(now).valueOf() * 1000;
    const {
      queryStartDate,
      queryEndDate,
    } = convertQueryParamsToFormDates({ start, end });
    this.setState({startValue: queryStartDate, endValue: queryEndDate})
  }

  handleScroll(){
    const top = document.body.scrollTop || document.documentElement.scrollTop;
    this.setState({'scrollTop': top});
  }

  disabledStartDate = (startValue) => {
    const endValue = this.state.endValue;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  disabledEndDate = (endValue) => {
    const startValue = this.state.startValue;
    if (!endValue || !startValue) {
      return false;
    }
    return endValue.valueOf() <= startValue.valueOf();
  }

  onChange = (field, value) => {
    dateForm[field] = value;
    this.setState({
      [field]: value,
    });
  }

  onStartChange = (value) => {
    this.onChange('startValue', value);
  }

  onEndChange = (value) => {
    this.onChange('endValue', value);
  }

  handleStartOpenChange = (open) => {
    if (!open) {
      this.setState({ endOpen: true });
    }
  }

  handleEndOpenChange = (open) => {
    this.setState({ endOpen: open });
  }

  render() {
    const {
      handleSubmit,
      selectedLookback,
      selectedService = '',
      services,
      submitting: disabled,
      reset,
      submitState,
    } = this.props;
    let getDateType = (type) => {
      switch (type) {
        case '10m':
          return '10minutes'
        case '30m':
          return '30minutes'
        case '1h':
          return '1hour'
        default:
          break;
      }
    }
    let hour = (type) => (<span className="hour"><FormattedMessage {...messages[getDateType(type)]}/></span>);
    const geLookback = ['10m','30m','1h'].includes(this.props.selectedLookback) ? hour(this.props.selectedLookback) : `${this.props.initialValues.startDate.format('YYYY-MM-DD HH:mm:ss')} ~ ${this.props.initialValues.endDate.format('YYYY-MM-DD HH:mm:ss')}`
    const displayValues = {
      service: this.props.initialValues.service,
      operation: this.props.initialValues.operation,
      tags: this.props.initialValues.tags,
      lookback: geLookback,
      minduration: this.props.initialValues.minDuration,
      maxduration: this.props.initialValues.maxDuration,
      limitresults: this.props.params.resultsLimit,
    };
    const namespace = localStorage.getItem('alauda_service_namespace');
    const selectedServicePayload = services.find(s => s.name === `${selectedService}.${namespace}` || s.name === selectedService);
    const opsForSvc = (selectedServicePayload && selectedServicePayload.operations) || [];
    const noSelectedService = selectedService === '' || !selectedService;
    const tz = selectedLookback === 'custom' ? new Date().toTimeString().replace(/^.*?GMT/, 'UTC') : null;
    const limitPlaceholder = this.props.intl.formatMessage({id: 'search.fields.limit_results'});
    return (
      <div>
        <Form layout="inline" onSubmit={handleSubmit}>
          <Row>
            <Col className="ant-row-fields" md={24}>
              <FormItem
                label={
                  <span>
                    <FormattedMessage {...messages.service}/>
                    <span className="SearchForm--labelCount">({services.length})</span>
                  </span>
                }
              >
                <Field
                  name="service"
                  component={AdaptedSelect}
                  placeholder={<FormattedMessage {...messages.selectService}/>}
                  onChange={submitState(disabled || noSelectedService)}
                >
                {/* <Option value="">{<FormattedMessage {...messages.selectService}/>}</Option> */}
                  {
                    services.map((item)=>(
                      <Option key={item.name} value={item.name}>{item.name.replace(`.${namespace}`, '')}</Option>
                    ))
                  }
                </Field>
              </FormItem>
              <FormItem
                label={
                  <span>
                    <FormattedMessage {...messages.operation}/>
                    <span className="SearchForm--labelCount">({opsForSvc ? opsForSvc.length : 0})</span>
                  </span>
                }
              >
                <Field
                  name="operation"
                  component={AdaptedSelect}
                  placeholder={<FormattedMessage {...messages.selectOperation}/>}
                >
                  {
                    ['all'].concat(opsForSvc).map((val)=>(
                      <Option key={val} value={val}>{val}</Option>
                    ))
                  }
                </Field>
              </FormItem>
              <FormItem
                label={
                  <div>
                    <FormattedMessage {...messages.tags}/>{' '}
                    
                  </div>
                }
              >
                <Field
                  name="tags"
                  component={AdaptedInput}
                  placeholder="http.status_code=200 error=true"
                  props={{ disabled }}
                />
                <Popover
                  placement="topLeft"
                  trigger="click"
                  title={[
                    <h3 key="title" className="SearchForm--tagsHintTitle">
                      Values should be in the{' '}
                      <a href="https://brandur.org/logfmt" rel="noopener noreferrer" target="_blank">
                        logfmt
                      </a>{' '}
                      format.
                    </h3>,
                    <ul key="info" className="SearchForm--tagsHintInfo">
                      <li>Use space for conjunctions</li>
                      <li>Values containing whitespace should be enclosed in quotes</li>
                    </ul>,
                  ]}
                  content={
                    <div>
                      <code className="SearchForm--tagsHintEg">
                        error=true db.statement=&quot;select * from User&quot;
                      </code>
                    </div>
                  }
                >
                  <IoHelp className="SearchForm--hintTrigger" />
                </Popover>
              </FormItem>
              <FormItem label={<FormattedMessage {...messages.lookback}/>}>
                <Field name="lookback" component={AdaptedSelect} props={{ disabled, defaultValue: '1h' }}>
                  <Option value="10m"><FormattedMessage {...messages['10minutes']}/></Option>
                  <Option value="30m"><FormattedMessage {...messages['30minutes']}/></Option>
                  <Option value="1h"><FormattedMessage {...messages['1hour']}/></Option>
                  <Option value="3h"><FormattedMessage {...messages['3hours']}/></Option>
                  <Option value="6h"><FormattedMessage {...messages['6hours']}/></Option>
                  <Option value="12h"><FormattedMessage {...messages['12hours']}/></Option>
                  <Option value="1d"><FormattedMessage {...messages['1day']}/></Option>
                  <Option value="3d"><FormattedMessage {...messages['3days']}/></Option>
                  <Option value="7d"><FormattedMessage {...messages['7days']}/></Option>
                  <Option value="30d"><FormattedMessage {...messages['30days']}/></Option>
                  <Option value="custom"><FormattedMessage {...messages.custom}/></Option>
                </Field>
              </FormItem>
            {selectedLookback === 'custom' &&
              <FormItem 
                label={<FormattedMessage {...messages.starttime}/>}
                className="search-field-date"
              >
                <Field
                  name="startDate"
                  component={AdaptedDatePikcer}
                  props={{ 
                    showTime: true,
                    format: 'YYYY-MM-DD HH:mm:ss',
                    value: this.state.startValue,
                    disabledDate: this.disabledStartDate,
                    onChange: this.onStartChange,
                    onOpenChange: this.handleStartOpenChange
                  }}
                />
                <Popover
                  placement="topLeft"
                  trigger="click"
                  content={
                    <h3 key="title" className="SearchForm--tagsHintTitle">
                      Times are expressed in {tz}
                    </h3>
                  }
                >
                  <IoHelp className="SearchForm--hintTrigger" />
                </Popover>
              </FormItem>
            }
            {selectedLookback === 'custom' &&
              <FormItem 
                label={<FormattedMessage {...messages.endtime}/>}
                className="search-field-date"
              >
                <Field
                  name="endDate"
                  component={AdaptedDatePikcer}
                  props={{ 
                    showTime: true,
                    format: 'YYYY-MM-DD HH:mm:ss',
                    disabledDate: this.disabledEndDate,
                    onChange: this.onEndChange,
                    value: this.state.endValue,
                    open: this.state.endOpen,
                    onOpenChange: this.handleEndOpenChange
                  }}
                />
                <Popover
                  placement="topLeft"
                  trigger="click"
                  content={
                    <h3 key="title" className="SearchForm--tagsHintTitle">
                      Times are expressed in {tz}
                    </h3>
                  }
                >
                  <IoHelp className="SearchForm--hintTrigger" />
                </Popover>
              </FormItem>
              }
              <FormItem label={<FormattedMessage {...messages.minduration}/>}>
                <Field
                  name="minDuration"
                  component={AdaptedInput}
                  placeholder="e.g. 1.2s, 100ms, 500us"
                  props={{ disabled }}
                />
              </FormItem>
              <FormItem label={<FormattedMessage {...messages.maxduration}/>}>
                <Field name="maxDuration" component={AdaptedInput} placeholder="e.g. 1.1s" props={{ disabled }} />
              </FormItem>
             
              <FormItem label={<FormattedMessage {...messages.limitresults}/>}>
                <Field
                  name="resultsLimit"
                  type="number"
                  component={AdaptedInput}
                  placeholder={limitPlaceholder}
                  props={{ disabled, min: 1, max: 1500 }}
                />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col md={24} className="ant-row-buttons">
              <Button 
                htmlType="submit" 
                disabled={disabled || noSelectedService} 
                data-test={markers.SUBMIT_BTN}
                type="primary"
              >
                <FormattedMessage {...messages.buttonSearch}/>
              </Button>
              <Button 
                htmlType="reset"
                onClick={reset}
              >
                <FormattedMessage {...messages.buttonReset}/>
              </Button>
            </Col>
          </Row>
        </Form>
        {this.state.scrollTop > 200 && (
          <div className="selected-conditions">
            <span className="selected-conditions-title"><FormattedMessage {...messages.conditions}/></span>
            {
              Object.keys(displayValues).map((key)=>!!displayValues[key] && (
                <span key={key} className="selected-condition-item">
                  <FormattedMessage {...messages[key]}/>{displayValues[key]}
                </span>
              ))
            }
          </div>
        )}
      </div>
    );
  }
}


SearchFormImpl.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool,
  services: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      operations: PropTypes.arrayOf(PropTypes.string),
    })
  ),
  selectedService: PropTypes.string,
  selectedLookback: PropTypes.string,
  intl: intlShape.isRequired
};

SearchFormImpl.defaultProps = {
  services: [],
  submitting: false,
  selectedService: null,
  selectedLookback: null,
};

export const searchSideBarFormSelector = formValueSelector('searchSideBar');

export function mapStateToProps(state) {
  const {
    service,
    limit,
    start,
    end,
    operation,
    tag: tagParams,
    tags: logfmtTags,
    maxDuration,
    minDuration,
    lookback,
    traceID: traceIDParams,
  } = queryString.parse(state.router.location.search);

  const lastSearch = store.get('lastSearch');
  let lastSearchService;
  let lastSearchOperation;

  if (lastSearch) {
    // last search is only valid if the service is in the list of services
    const { operation: lastOp, service: lastSvc } = lastSearch;
    if (lastSvc && lastSvc !== '') {
      if (state.services.services.includes(lastSvc)) {
        lastSearchService = lastSvc;
        if (lastOp && lastOp !== '') {
          const ops = state.services.operationsForService[lastSvc];
          if (lastOp === 'all' || (ops && ops.includes(lastOp))) {
            lastSearchOperation = lastOp;
          }
        }
      }
    }
  }

  const {
    queryStartDate,
    queryEndDate,
  } = convertQueryParamsToFormDates({ start, end });

  let tags;
  // continue to parse tagParams to remain backward compatible with older URLs
  // but, parse to logfmt format instead of the former "key:value|k2:v2"
  if (tagParams) {
    // eslint-disable-next-line no-inner-declarations
    function convFormerTag(accum, value) {
      const parts = value.split(':', 2);
      const key = parts[0];
      if (key) {
        // eslint-disable-next-line no-param-reassign
        accum[key] = parts[1] == null ? '' : parts[1];
        return true;
      }
      return false;
    }

    let data;
    if (Array.isArray(tagParams)) {
      data = tagParams.reduce((accum, str) => {
        convFormerTag(accum, str);
        return accum;
      }, {});
    } else if (typeof tagParams === 'string') {
      const target = {};
      data = convFormerTag(target, tagParams) ? target : null;
    }
    if (data) {
      try {
        tags = logfmtStringify(data);
      } catch (_) {
        tags = 'Parse Error';
      }
    } else {
      tags = 'Parse Error';
    }
  }
  if (logfmtTags) {
    let data;
    try {
      data = JSON.parse(logfmtTags);
      tags = logfmtStringify(data);
    } catch (_) {
      tags = 'Parse Error';
    }
  }
  let traceIDs;
  if (traceIDParams) {
    traceIDs = traceIDParams instanceof Array ? traceIDParams.join(',') : traceIDParams;
  }
  // from parent page
  const storedCondition = localStorage.getItem('condition');
  const conditions = storedCondition ? JSON.parse(storedCondition) : { service: '', lookback: '' };
  if(!state.services.services.includes(conditions.service)){
    conditions.service = '';
  }
  const { startDate, endDate } = getCurrendTime();
  
  return {
    destroyOnUnmount: false,
    initialValues: {
      service: service || conditions.service || lastSearchService,
      resultsLimit: limit || DEFAULT_LIMIT,
      lookback: lookback || conditions.lookback || DEFAULT_LOOKBACK,
      startDate: queryStartDate || startDate,
      endDate: queryEndDate || endDate,
      operation: operation || lastSearchOperation || DEFAULT_OPERATION,
      tags,
      minDuration: minDuration || null,
      maxDuration: maxDuration || null,
      traceIDs: traceIDs || null,
    },
    params: {
      service: service || conditions.service || lastSearchService,
      resultsLimit: limit,
      lookback: lookback || conditions.lookback || DEFAULT_LOOKBACK,
      startDate: queryStartDate || startDate,
      endDate: queryEndDate || endDate,
      operation: operation || lastSearchOperation || DEFAULT_OPERATION,
      tags,
      minDuration: minDuration || null,
      maxDuration: maxDuration || null,
      traceIDs: traceIDs || null,
    },
    selectedService: searchSideBarFormSelector(state, 'service'),
    selectedLookback: searchSideBarFormSelector(state, 'lookback'),
  };
}

function mapDispatchToProps(dispatch) {
  const { searchTraces } = bindActionCreators(jaegerApiActions, dispatch);
  return {
    onSubmit: fields => submitForm(fields, searchTraces),
  };
}

function getCurrendTime() {
  const now = new Date();
  const startDate =
      moment(now)
        .subtract(parseInt('1h', 10), 'h')
  const endDate = moment(now);
  return {startDate, endDate}
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(
  reduxForm({
    form: 'searchSideBar',
  })(SearchFormImpl))
);
