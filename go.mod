module github.com/jaegertracing/jaeger

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/PuerkitoBio/purell v1.1.1-0.20171117214151-1c4bec281e4b // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/Shopify/sarama v1.16.0
	github.com/Shopify/toxiproxy v2.1.4+incompatible // indirect
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/apache/thrift v0.0.0-20151001171628-53dd39833a08
	github.com/asaskevich/govalidator v0.0.0-20171128153514-852d82c746b2 // indirect
	github.com/beorn7/perks v0.0.0-20180321164747-3a771d992973 // indirect
	github.com/bmizerany/perks v0.0.0-20141205001514-d9a9656a3a4b // indirect
	github.com/bsm/sarama-cluster v2.1.13+incompatible
	github.com/codahale/hdrhistogram v0.0.0-20161010025455-3a0bb77429bd // indirect
	github.com/crossdock/crossdock-go v0.0.0-20160816171116-049aabb0122b
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/eapache/go-resiliency v1.1.0 // indirect
	github.com/eapache/go-xerial-snappy v0.0.0-20160609142408-bb955e01b934 // indirect
	github.com/eapache/queue v1.1.1-0.20180227141424-093482f3f8ce // indirect
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-kit/kit v0.5.0 // indirect
	github.com/go-openapi/analysis v0.0.0-20171208011258-5c7230aa5ab8 // indirect
	github.com/go-openapi/errors v0.0.0-20170426151106-03cfca65330d
	github.com/go-openapi/jsonpointer v0.0.0-20170102174223-779f45308c19 // indirect
	github.com/go-openapi/jsonreference v0.0.0-20161105162150-36d33bfe519e // indirect
	github.com/go-openapi/loads v0.0.0-20171207192234-2a2b323bab96
	github.com/go-openapi/runtime v0.0.0-20171207053002-55d76b231921
	github.com/go-openapi/spec v0.0.0-20171206193454-01738944bdee
	github.com/go-openapi/strfmt v0.0.0-20170822153411-610b6cacdcde
	github.com/go-openapi/swag v0.0.0-20171111214437-cf0bdb963811
	github.com/go-openapi/validate v0.0.0-20171117174350-d509235108fc
	github.com/gocql/gocql v0.0.0-20200203083758-81b8263d9fe5
	github.com/gogo/googleapis v1.0.1-0.20180501115203-b23578765ee5
	github.com/gogo/protobuf v0.0.0-20171130202109-fd9a4790f396
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/protobuf v1.2.0
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/handlers v0.0.0-20161206055144-3a5767ca75ec
	github.com/gorilla/mux v1.3.0
	github.com/grpc-ecosystem/grpc-gateway v1.3.1-0.20180312001938-58f78b988bc3
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/kr/pretty v0.1.0
	github.com/mailru/easyjson v0.0.0-20171120080333-32fa128f234d // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.1 // indirect
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/opentracing-contrib/go-stdlib v0.0.0-20171029140428-b1a47cfbdd75
	github.com/opentracing/opentracing-go v1.0.2
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/pierrec/lz4 v2.0.2+incompatible // indirect
	github.com/pierrec/xxHash v0.1.5 // indirect
	github.com/pkg/errors v0.8.0
	github.com/prashantv/protectmem v0.0.0-20171002184600-e20412882b3a // indirect
	github.com/prometheus/client_golang v0.8.0
	github.com/prometheus/client_model v0.0.0-20171117100541-99fa1f4be8e5 // indirect
	github.com/prometheus/common v0.0.0-20180326160409-38c53a9f4bfc // indirect
	github.com/prometheus/procfs v0.0.0-20180321230812-780932d4fbbe // indirect
	github.com/rakyll/statik v0.1.6
	github.com/rcrowley/go-metrics v0.0.0-20180503174638-e2704e165165 // indirect
	github.com/spf13/cobra v0.0.1
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.2.1
	github.com/streadway/quantile v0.0.0-20150917103942-b0c588724d25 // indirect
	github.com/stretchr/objx v0.1.2-0.20180825064932-ef50b0de2877 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/uber-go/atomic v1.3.1 // indirect
	github.com/uber/jaeger-client-go v2.15.0+incompatible
	github.com/uber/jaeger-lib v1.5.0
	github.com/uber/tchannel-go v1.1.0
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1
	golang.org/x/net v0.0.0-20181011144130-49bb7cea24b1
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	google.golang.org/genproto v0.0.0-20171212231943-a8101f21cf98 // indirect
	google.golang.org/grpc v1.11.0
	gopkg.in/mgo.v2 v2.0.0-20160818020120-3f83fa500528 // indirect
	gopkg.in/olivere/elastic.v5 v5.0.53
	gopkg.in/yaml.v2 v2.2.4
)
